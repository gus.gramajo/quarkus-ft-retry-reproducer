package com.repro.core;

import io.quarkus.security.UnauthorizedException;
import io.quarkus.vertx.ConsumeEvent;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.eclipse.microprofile.faulttolerance.Retry;

@ApplicationScoped
public class APIClientVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(com.repro.core.APIClientVerticle.class);
    private WebClient client;

    @Override
    public void start() throws Exception {
        super.start();
        WebClientOptions options = new WebClientOptions();

        // development only
        options.setTrustAll(true).setVerifyHost(false);
        client = WebClient.create(vertx, options);
    }

    @ConsumeEvent("sendRequest")
    @Retry(maxRetries = 4, retryOn = RuntimeException.class, delay = 2000)
    public CompletionStage<JsonObject> sendRequest(String url) {
        var future = new CompletableFuture<JsonObject>();

        logger.info("sendRequest: " + url);
        client.getAbs(url)
            .send()
            .onComplete(res -> {
                throw new RuntimeException("Unauthorized request");
            });

        return future;
    }

}