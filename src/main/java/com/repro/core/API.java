package com.repro.core;

import io.quarkus.vertx.web.Route;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.quarkus.vertx.web.Route.HttpMethod;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import javax.inject.Inject;

public class API {
    private static final Logger logger = LoggerFactory.getLogger(API.class);

    private static final DeliveryOptions DOPTS = new DeliveryOptions();

    @Inject
    EventBus bus;

    @Route(path = "/retryWorks", methods = HttpMethod.GET, type = Route.HandlerType.NORMAL)
    public void retryWorks(RoutingContext rc) {
        bus.request("sendRequest", "http://unexistent_domain.com", DOPTS, onResult(rc));
    }

    @Route(path = "/retrySkipped", methods = HttpMethod.GET, type = Route.HandlerType.NORMAL)
    public void retrySkipped(RoutingContext rc) {
        bus.request("sendRequest", "https://pokeapi.co/api/v2/generation/", DOPTS, onResult(rc));
    }

    protected Handler<AsyncResult<Message<Object>>> onResult(RoutingContext rc) {
        return ar -> {

            if (ar.succeeded()) {
                rc.response().putHeader("Content-Type", "application/json");
                rc.response().end(new JsonObject()
                        .put("success", true).encodePrettily());
            } else {
                rc.response().setStatusCode(502);
                rc.end();
            }
        };
    }

}