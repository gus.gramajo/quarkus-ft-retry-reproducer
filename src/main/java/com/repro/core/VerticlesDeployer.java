package com.repro.core;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.StartupEvent;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;

@ApplicationScoped
public class VerticlesDeployer {
    private static final Logger logger = LoggerFactory.getLogger(VerticlesDeployer.class);

    public void init(@Observes StartupEvent e, Vertx vertx, Instance<AbstractVerticle> verticles) {
        try {
            logger.info("System start...");

            for (AbstractVerticle verticle : verticles) {
                vertx.deployVerticle(verticle, ar -> {
                    if (!ar.succeeded()) {
                         logger.fatal("System start fail", ar.cause());
                        Quarkus.asyncExit();
                    }
                });
            }

        } catch (Exception exc) {
            logger.error(exc);
        }
    }
}
